<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table){
            $table->increments('id');
            $table->integer('driver_id')->unsigned();
            $table->date('date');
            $table->string('title');
            $table->integer('car_id')->unsigned();
            $table->time('time_from_terminal');
            $table->integer('run_at_start');
            $table->time('time_at_client');
            $table->integer('loading_time');
            $table->time('time_from_client');
            $table->time('time_at_terminal');
            $table->integer('run_at_terminal');
            $table->timestamps();

            $table->foreign('car_id')->references('id')->on('cars')->onDellete('cascade');
            $table->foreign('driver_id')->references('id')->on('users')->onDellete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trips');
    }
}
