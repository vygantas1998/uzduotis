<?php

use App\Car;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(UsersTableSeeder::class);
         $this->call(CarsTableSeeder::class);

        Model::reguard();
    }
}
class UsersTableSeeder extends Seeder {
    public function run(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        User::insert([
            [
                'name' => 'Vairuotojas',
                'role' => 'driver',
                'email' => 'driver@driver',
                'password' => bcrypt('driver'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Administratorius',
                'role' => 'admin',
                'email' => 'admin@admin',
                'password' => bcrypt('admin'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
class CarsTableSeeder extends Seeder {
    public function run(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Car::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Car::insert([
            [
                'name' => 'Opel',
                'neutral' => '9',
                'loading' => '6',
                'driving' => '10',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}

