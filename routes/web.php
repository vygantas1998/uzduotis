<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cars/list', ['uses' => 'CarsController@index', 'as' => 'cars.list']);
Route::get('/cars/create', ['uses' => 'CarsController@create', 'as' => 'cars.create']);
Route::post('/cars/store', ['uses' => 'CarsController@store', 'as' => 'cars.store']);
Route::get('/cars/remove/{car}', ['uses' => 'CarsController@remove', 'as' => 'cars.remove']);
Route::get('/cars/edit/{car}', ['uses' => 'CarsController@edit', 'as' => 'cars.edit']);
Route::post('/cars/update/{car}', ['uses' => 'CarsController@update', 'as' => 'cars.update']);

Route::get('/trips/list', ['uses' => 'TripsController@index', 'as' => 'trips.list']);
Route::get('/trips/create', ['uses' => 'TripsController@create', 'as' => 'trips.create']);
Route::post('/trips/store', ['uses' => 'TripsController@store', 'as' => 'trips.store']);
Route::get('/trips/remove/{trip}', ['uses' => 'TripsController@remove', 'as' => 'trips.remove']);
Route::get('/trips/edit/{trip}', ['uses' => 'TripsController@edit', 'as' => 'trips.edit']);
Route::post('/trips/update/{trip}', ['uses' => 'TripsController@update', 'as' => 'trips.update']);

Route::get('/reports', ['uses' => 'ReportsController@index', 'as' => 'reports.index']);
Route::post('/reports/show', ['uses' => 'ReportsController@show', 'as' => 'reports.show']);