<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\TripRequest;
use App\Trip;
use Illuminate\Support\Facades\Auth;

class TripsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:driver');
    }

    public function index()
    {
        $trips = Trip::where('driver_id', Auth::user()->id)->get();
        return view('trips.list', compact('trips'));
    }
    public function create(){
        $cars = Car::all();
        return view('trips.create', compact('cars'));
    }
    public function store(TripRequest $request)
    {
        $request->merge(['driver_id' => Auth::user()->id]);
        Trip::create($request->all());
        return redirect()->route('trips.list');
    }
    public function remove(Trip $trip){
        $trip->delete();
        return redirect()->back();

    }
    public function edit(Trip $trip){
        $cars = Car::all();
        return view('trips.edit', compact('trip', 'cars'));
    }
    public function update(Trip $trip, TripRequest $request){
        $trip->update($request->all());
        return redirect()->route('trips.list');
    }
}
