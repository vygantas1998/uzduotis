<?php

namespace App\Http\Controllers;

use App\Car;
use App\Trip;
use App\User;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }
    public function index()
    {
        $users = User::where('role', 'driver')->get();
        return view('reports.index', compact('users'));
    }
    public function show(Request $request)
    {
        $this->validate($request,['date' => 'required|date']);
        $reports = Trip::where([['date', '=', $request->date], ['driver_id', '=', $request->driver_id]])->get();
            foreach ($reports as $report) {
                $neutral = Car::where('id', $report->car_id)->first()->neutral;
                $loading = Car::where('id', $report->car_id)->first()->loading;
                $driving = Car::where('id', $report->car_id)->first()->driving;
                $distance = $report->run_at_terminal - $report->run_at_start;
                $fuel = (strtotime($report->time_at_client) - strtotime($report->time_from_terminal)) / 3600 * $driving +
                    $report->loading_time / 60 * $loading +
                    (strtotime($report->time_from_client) - strtotime($report->time_at_client) - $report->loading_time * 60) / 3600 * $neutral +
                    (strtotime($report->time_at_terminal) - strtotime($report->time_from_client)) / 3600 * $driving;
                array_add($report, 'distance', round($distance, 1));
                array_add($report, 'fuel', round($fuel, 1));
            }
        return view('reports.show', compact('reports', 'request'));
    }
}
