<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\CarRequest;

class CarsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $cars = Car::all();
        return view('cars.list', compact('cars'));
    }
    public function create(){
        return view('cars.create');
    }
    public function store(CarRequest $request)
    {
        Car::create($request->all());
        return redirect('cars/list');
    }
    public function remove(Car $car){
        $car->delete();
        return redirect()->back();

    }
    public function edit(Car $car){
        return view('cars.edit', compact('car'));
    }
    public function update(Car $car, CarRequest $request){
        $car->update($request->all());
        return redirect()->route('cars.list');
    }
}
