<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'title' => 'required|min:3|max:100',
            'car_id' => 'required',
            'time_from_terminal' => 'required',
            'run_at_start' => 'required|integer',
            'time_at_client' => 'required',
            'loading_time' => 'required|integer',
            'time_from_client' => 'required',
            'time_at_terminal' => 'required',
            'run_at_terminal' => 'required|integer',
        ];
    }
}
