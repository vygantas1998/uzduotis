<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'date', 'title', 'car_id', 'time_from_terminal', 'run_at_start', 'time_at_client',
        'loading_time', 'time_from_client', 'time_at_terminal', 'run_at_terminal', 'driver_id'
    ];

}
