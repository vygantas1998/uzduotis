@extends('layouts.panel')
@section('styles')
    <link rel="stylesheet" href="{{asset('css\dataTables.bootstrap.css')}}">
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Vairuotojas: {{ \App\User::where('id', $request->driver_id)->first()->name }}, {{$request->date}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nr.</th>
                        <th>Data</th>
                        <th>Maršrutas</th>
                        <th>Išvyko iš terminalo</th>
                        <th>Spidometro parodymai išvykstant</th>
                        <th>Atvyko pas klientą</th>
                        <th>Iškrovimas(min)</th>
                        <th>Išvyko iš kliento</th>
                        <th>Grįžo į terminalą</th>
                        <th>Spidometro parodymai grįžus</th>
                        <th>Atstumas(km)</th>
                        <th>Kuras(l)</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reports as $trip)
                    <tr>
                        <td>{{$trip->id}}</td>
                        <td>{{$trip->date}}</td>
                        <td>{{$trip->title}}</td>
                        <td>{{$trip->time_from_terminal}}</td>
                        <td>{{$trip->run_at_start}}</td>
                        <td>{{$trip->time_at_client}}</td>
                        <td>{{$trip->loading_time}}</td>
                        <td>{{$trip->time_from_client}}</td>
                        <td>{{$trip->time_at_terminal}}</td>
                        <td>{{$trip->run_at_terminal}}</td>
                        <td>{{$trip->distance}}</td>
                        <td>{{$trip->fuel}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@section('scripts')
    <!-- DataTables -->
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $("#list").DataTable();
        });
    </script>
@endsection