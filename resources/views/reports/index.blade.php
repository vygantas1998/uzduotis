@extends('layouts.panel')

@section('styles')
    <link rel="stylesheet" href="{{asset('css/datepicker3.css')}}">
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Ataskaitos</h3>
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('reports.show') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                    <label for="driver_id">Vairuotojas:</label>
                    <select id="driver_id" class="form-control" name="driver_id">
                        @foreach($users as $user)
                            <option value="{{$user->id}}" {{!empty($errors->first()) ? (old('driver_id')==$user->id ? 'selected="selected"' : "") : ""}}>{{$user->name}}, Id:{{$user->id}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="datepicker">Data:</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="date" type="text" class="form-control pull-right" id="datepicker" value="{{ empty(old('date')) ? \Carbon\Carbon::today()->format('Y-m-d') : old('date')}}">
                    </div>
                    @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    Rodyti
                </button>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/locales/bootstrap-datepicker.lt.js')}}"></script>
    <script>
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            language: 'lt',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            todayBtn: "linked"
        });
    </script>
@endsection