@extends('layouts.panel')
@section('styles')
    <link rel="stylesheet" href="{{asset('css\dataTables.bootstrap.css')}}">
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Mašinų sarašas</h3>
            <span class="pull-right"><a href="{{route('cars.create')}}" class="btn btn-primary">Sukurti naują</a></span>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Pavadinimas</th>
                        <th>Sąnaudos stovint (l/h)</th>
                        <th>Sąnaudos iškraunant (l/h)</th>
                        <th>Sąnaudos važiuojant (l/h)</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cars as $car)
                    <tr>
                        <td>{{$car->id}}</td>
                        <td>{{$car->name}}</td>
                        <td>{{$car->neutral}}</td>
                        <td>{{$car->loading}}</td>
                        <td>{{$car->driving}}</td>
                        <td><a href="{{route('cars.edit', $car->id)}}" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
                        <a href="{{route('cars.remove', $car->id)}}" class="btn btn-primary"><span class="fa fa-remove"></span></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@section('scripts')
    <!-- DataTables -->
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $("#list").DataTable();
        });
    </script>
@endsection