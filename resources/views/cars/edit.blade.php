@extends('layouts.panel')
@section('content')
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Redaguoti mašinos duomenis</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" role="form" method="POST" action="{{ route('cars.update', $car->id) }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Padavadinimas:</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ empty($errors->first()) ? $car->name : old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('neutral') ? ' has-error' : '' }}">
                    <label for="neutral">Sąnaudos stovint (l/h):</label>
                    <input id="neutral" type="number" class="form-control" name="neutral" value="{{ empty($errors->first()) ? $car->neutral : old('neutral') }}">
                    @if ($errors->has('neutral'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('neutral') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('loading') ? ' has-error' : '' }}">
                    <label for="loading">Sąnaudos iškraunant (l/h):</label>
                    <input id="loading" type="number" class="form-control" name="loading" value="{{ empty($errors->first()) ? $car->loading : old('loading') }}">
                    @if ($errors->has('loading'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('loading') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('driving') ? ' has-error' : '' }}">
                    <label for="driving">Sąnaudos važiuojant (l/h):</label>
                    <input id="driving" type="number" class="form-control" name="driving" value="{{ empty($errors->first()) ? $car->driving : old('driving') }}">
                    @if ($errors->has('driving'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('driving') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    Patvirtinti
                </button>
            </div>
        </form>
    </div>
    <!-- /.box -->
@endsection