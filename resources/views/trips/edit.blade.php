@extends('layouts.panel')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-timepicker.min.css')}}">
@endsection
@section('content')
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Redaguoti reisą</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" role="form" method="POST" action="{{ route('trips.update', $trip->id) }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="datepicker">Data:</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="date" type="text" class="form-control pull-right" id="datepicker" value="{{ empty($errors->first()) ? $trip->date : old('date') }}">
                    </div>
                    @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Padavadinimas:</label>
                    <input id="title" type="text" class="form-control" name="title" value="{{ empty($errors->first()) ? $trip->title : old('title') }}">
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('car_id') ? ' has-error' : 'car_id' }}">
                    <label for="car_id">Mašina:</label>
                    <select id="car_id" class="form-control" name="car_id">
                        @foreach($cars as $car)
                            <option value="{{$car->id}}"{{!empty($errors->first()) ? (old('car_id')==$car->id ? 'selected="selected"' : "") : (($trip->car_id==$car->id) ? 'selected="selected"' : "")}}>
                                {{$car->name}}, Id:{{$car->id}}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('car_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('car_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group {{ $errors->has('time_from_terminal') ? ' has-error' : '' }}">
                        <label for="time_from_terminal">Išvykimas iš terminalo:</label>
                        <div class="input-group">
                            <input id="time_from_terminal" type="text" name="time_from_terminal" class="form-control timepicker" value="{{ empty($errors->first()) ? $trip->time_from_terminal : old('time_from_terminal') }}">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        @if ($errors->has('time_from_terminal'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('time_from_terminal') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('run_at_start') ? ' has-error' : '' }}">
                    <label for="run_at_start">Spidometro parodymai išvykstant:</label>
                    <input id="run_at_start" type="number" class="form-control" name="run_at_start" value="{{ empty($errors->first()) ? $trip->run_at_start : old('run_at_start') }}">
                    @if ($errors->has('run_at_start'))
                        <span class="help-block">
                            <strong>{{ $errors->first('run_at_start') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group {{ $errors->has('time_at_client') ? ' has-error' : '' }}">
                        <label for="time_at_client">Atvykimas pas klientą:</label>
                        <div class="input-group">
                            <input id="time_at_client" type="text" name="time_at_client" class="form-control timepicker" value="{{ empty($errors->first()) ? $trip->time_at_client : old('time_at_client') }}">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        @if ($errors->has('time_at_client'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('time_at_client') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('loading_time') ? ' has-error' : '' }}">
                    <label for="loading_time">Iškrovimo trukmė(min):</label>
                    <input id="loading_time" type="number" class="form-control" name="loading_time" value="{{ empty($errors->first()) ? $trip->loading_time : old('loading_time') }}">
                    @if ($errors->has('loading_time'))
                        <span class="help-block">
                            <strong>{{ $errors->first('loading_time') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group {{ $errors->has('time_from_client') ? ' has-error' : '' }}">
                        <label for="time_from_client">Išvykimas iš kliento:</label>
                        <div class="input-group">
                            <input id="time_from_client" type="text" name="time_from_client" class="form-control timepicker" value="{{ empty($errors->first()) ? $trip->time_from_client : old('time_from_client') }}">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        @if ($errors->has('time_from_client'))
                            <span class="help-block">
                                <strong>{{ $errors->first('time_from_client') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group {{ $errors->has('time_at_terminal') ? ' has-error' : '' }}">
                        <label for="time_at_terminal">Grįžimas į terminalą:</label>
                        <div class="input-group">
                            <input id="time_at_terminal" type="text" name="time_at_terminal" class="form-control timepicker" value="{{ empty($errors->first()) ? $trip->time_at_terminal : old('time_at_terminal') }}">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        @if ($errors->has('time_at_terminal'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('time_at_terminal') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('run_at_terminal') ? ' has-error' : '' }}">
                    <label for="run_at_start">Spidometro parodymai grįžus:</label>
                    <input id="run_at_start" type="number" class="form-control" name="run_at_terminal" value="{{ empty($errors->first()) ? $trip->run_at_terminal : old('run_at_terminal') }}">
                    @if ($errors->has('run_at_terminal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('run_at_terminal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    Patvirtinti
                </button>
            </div>
        </form>
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/locales/bootstrap-datepicker.lt.js')}}"></script>
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <script>
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            language: 'lt',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            todayBtn: "linked"
        });
        //Timepicker
        $(".timepicker").timepicker({
            showMeridian: false,
            minuteStep: 1,
            showInputs: false
        });
    </script>
@endsection