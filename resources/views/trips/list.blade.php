@extends('layouts.panel')
@section('styles')
    <link rel="stylesheet" href="{{asset('css\dataTables.bootstrap.css')}}">
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Reisų sąrašas</h3>
            <span class="pull-right"><a href="{{route('trips.create')}}" class="btn btn-primary">Sukurti naują</a></span>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Data</th>
                        <th>Pavadinimas</th>
                        <th>Mašina</th>
                        <th>Išvykimas iš terminalo</th>
                        <th>Spidometro parodymai išvykstant</th>
                        <th>Atvykimas pas klientą</th>
                        <th>Iškrovimo trukmė(min)</th>
                        <th>Išvykimas iš kliento</th>
                        <th>Grįžimas į terminalą</th>
                        <th>Spidometro parodymai grįžus</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($trips as $trip)
                    <tr>
                        <td>{{$trip->id}}</td>
                        <td>{{$trip->date}}</td>
                        <td>{{$trip->title}}</td>
                        <td>{{\App\Car::where('id',$trip->car_id)->get()->first()->name}}</td>
                        <td>{{$trip->time_from_terminal}}</td>
                        <td>{{$trip->run_at_start}}</td>
                        <td>{{$trip->time_at_client}}</td>
                        <td>{{$trip->loading_time}}</td>
                        <td>{{$trip->time_from_client}}</td>
                        <td>{{$trip->time_at_terminal}}</td>
                        <td>{{$trip->run_at_terminal}}</td>
                        <td><a href="{{route('trips.edit', $trip->id)}}" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
                        <a href="{{route('trips.remove', $trip->id)}}" class="btn btn-primary"><span class="fa fa-remove"></span></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@section('scripts')
    <!-- DataTables -->
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $("#list").DataTable();
        });
    </script>
@endsection